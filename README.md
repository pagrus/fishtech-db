# FishTech DB

This is a SQL version of [Peter Howgate's](http://www.iafi.net/page-1845729) FishTech DB, which can be found in its original (MS Excel) format on the Oregon State University's Seafood Network Information Center [Publications page](http://seafood.oregonstate.edu/seafood-network/news-publications/publications).

## About the DB

From Dr. Howgate's statement in the original Excel file:

> FishTechDB is a bibliographical database of topics related to fish technology. Peter Howgate who worked at Torry Research Station and was Head of the Quality Studies Section when he retired in December 1989 has generously donated his time and efforts in creating and making this database available as a public service to the global seafood community via this web site.

> The database “Fish Technology” is intended to cover practices for the post harvest handling and processing of all types of animal products obtained from the aquatic environment (in the usual convention; not including mammalian species), with particular emphasis on references related to measurement of quality, quality control and quality assurance, composition, and public health.

> The bibliography is not intended to cover the basic sciences of biology, microbiology, chemistry, biochemistry, physics and engineering associated with fish technology. The bibliography is based on that compiled by the library of Torry Research Station, Aberdeen, UK, between 1984 and 1994, and called TORLIB. The Station was closed in 1994 and the remnants of its duties transferred to other government departments and TORLIB was not maintained.

> Peter obtained a copy of TORLIB in a form usable on a PC and converted the contents to a Microsoft Excel file. TORLIB contained almost 36,000 entries, but many of these included references to topics which were not germane to the objectives of the database as summarised above and it was scanned to select references which he considered were relevant. This left a databse of about 8,000 entries. The references were formatted to a uniform style, journal names expanded to full listings, and abstracts added where these could be found, (the originals mostly had keywords). Further references were added from Peter's own collection, which expanded the time span of the coverage of the database, and periodically he has added further references so that the database now, Version 10, December 2015, contains more than 10,600 entries. Seafood Network Information Center (SeafoodNIC) hosts the database.

The remainder of the original “About” worksheet in the Excel file describes the format and usage of the Excel version, which is not relevant here.

This version of the DB separates the citations and authors into two tables. Future versions may attempt to add tables for publications or other attributes. It is likely that there are errors, but hopefully these will be corrected as they are discovered.

## Files